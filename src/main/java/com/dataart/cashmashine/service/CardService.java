package com.dataart.cashmashine.service;

import com.dataart.cashmashine.domain.CreditCard;
import com.dataart.cashmashine.dto.BalanceAndLastOperationDto;
import com.dataart.cashmashine.utils.PinCodeCheckResult;
import org.springframework.stereotype.Service;

/**
 * Created by redwhite on 10.11.2015.
 */
@Service
public interface CardService {

    CreditCard findNonBlockedCard(String cardNumber);

    PinCodeCheckResult checkPinForCard(String pinCode, String cardNumber);

    BalanceAndLastOperationDto withDraw(long balance, long amount, String cardNumber);

    void trackBalanceRequest(String cardNumber);
}
