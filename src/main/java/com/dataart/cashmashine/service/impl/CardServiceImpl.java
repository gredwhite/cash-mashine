package com.dataart.cashmashine.service.impl;

import com.dataart.cashmashine.dao.CardDao;
import com.dataart.cashmashine.domain.CreditCard;
import com.dataart.cashmashine.domain.Operation;
import com.dataart.cashmashine.domain.OperationType;
import com.dataart.cashmashine.dto.BalanceAndLastOperationDto;
import com.dataart.cashmashine.service.CardService;
import com.dataart.cashmashine.utils.PinCodeCheckResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by redwhite on 10.11.2015.
 */
@Transactional
@Service
public class CardServiceImpl implements CardService {
    private static final Logger LOG = LoggerFactory.getLogger(CardServiceImpl.class);

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private CardDao cardDao;

    @Override
    public CreditCard findNonBlockedCard(String cardNumber) {
        return cardDao.findNonBlockedCard(cardNumber);
    }

    /**
     * Checks that pin for card correct. If pin entered wrong counter increments.
     * If counter has reached - card blocks.
     *
     * @param pinCode
     * @param cardNumber
     * @return PinCodeCheckResult.BLOCKED if card is blocked
     * PinCodeCheckResult.INVALID if card is invalid
     * PinCodeCheckResult.VALID if card is valid
     */
    @Override
    public PinCodeCheckResult checkPinForCard(String pinCode, String cardNumber) {
        String pinByCardNumber = cardDao.getPinByCardNumber(cardNumber);
        boolean validPin = bCryptPasswordEncoder.matches(pinCode, pinByCardNumber);
        if (!validPin) {
            boolean blocked = cardDao.incrementWrongPinCodeCountAttemptsAndBlockIfLimitExceeded(cardNumber);
            if (blocked) {
                return PinCodeCheckResult.BLOCKED;
            }
            return PinCodeCheckResult.INVALID;
        }
        cardDao.resetWrongPinCodeCountAttempts(cardNumber);
        return PinCodeCheckResult.VALID;
    }

    /**
     * Methods tries to withDraw balance. If balance is not enough withDraw doesn't happen.
     *
     * @param balance
     * @param amount
     * @param cardNumber
     * @return BalanceAndLastOperationDto and null if balance is not enoug
     */
    @Override
    public BalanceAndLastOperationDto withDraw(long balance, long amount, String cardNumber) {
        if (balance >= amount) {
            CreditCard card = cardDao.findNonBlockedCard(cardNumber);
            long newBalance = balance - amount;
            card.setBalance(newBalance);
            Operation operation = configureWithDrawOperation(amount, card);
            cardDao.saveCard(card);
            LOG.info("Balance changed from [{}] to [{}] for card [{}]", balance, newBalance, cardNumber);
            return prepareBalanceAndLastOperationDto(newBalance, operation);
        }
        LOG.warn("Balance not updated for card [{}] because amount too large", cardNumber);
        return null;
    }

    @Override
    public void trackBalanceRequest(String cardNumber) {
        CreditCard nonBlockedCard = cardDao.findNonBlockedCard(cardNumber);
        Operation operation = new Operation();
        operation.setOperationType(OperationType.WATCH_BALANCE);
        nonBlockedCard.getOperations().add(operation);
        cardDao.saveCard(nonBlockedCard);

    }

    private BalanceAndLastOperationDto prepareBalanceAndLastOperationDto(long newBalance, Operation operation) {
        BalanceAndLastOperationDto balanceAndLastOperationDto = new BalanceAndLastOperationDto();
        balanceAndLastOperationDto.setOperation(operation);
        balanceAndLastOperationDto.setNewBalance(newBalance);
        return balanceAndLastOperationDto;
    }

    private Operation configureWithDrawOperation(long amount, CreditCard card) {
        Operation operation = new Operation();
        operation.setMoneyAmount(amount);
        operation.setOperationType(OperationType.WITH_DRAW);
        card.getOperations().add(operation);
        return operation;
    }
}
