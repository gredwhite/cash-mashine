package com.dataart.cashmashine.dto;

import com.dataart.cashmashine.domain.Operation;

/**
 * Dto with balance and operation. Created as result of withDraw method result.
 */
public class BalanceAndLastOperationDto {
    private long newBalance;
    private Operation operation;

    public long getNewBalance() {
        return newBalance;
    }

    public void setNewBalance(long newBalance) {
        this.newBalance = newBalance;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}
