package com.dataart.cashmashine.dao.impl;

import com.dataart.cashmashine.dao.CardDao;
import com.dataart.cashmashine.domain.CardState;
import com.dataart.cashmashine.domain.CreditCard;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by redwhite on 11.11.2015.
 */
@Repository
public class CardDaoImpl implements CardDao {
    private static final Logger LOG = LoggerFactory.getLogger(CardDaoImpl.class);
    public static final int WRONG_PIN_ATTEMPTS_COUNT = 4;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public CreditCard findNonBlockedCard(String cardNumber) {
        Session currentSession = sessionFactory.getCurrentSession();
        return (CreditCard) currentSession.createQuery("select cc from CreditCard as cc join cc.cardState as cs where cc.cardNumber=:cardNumber and cs.blocked is false")
                .setParameter("cardNumber", cardNumber)
                .uniqueResult();
    }

    @Override
    public String getPinByCardNumber(String cardNumber) {
        Session currentSession = sessionFactory.getCurrentSession();
        return (String) currentSession.createQuery("select cc.pinCode from CreditCard cc where cc.cardNumber=:cardNumber")
                .setParameter("cardNumber", cardNumber)
                .uniqueResult();

    }

    @Override
    public boolean incrementWrongPinCodeCountAttemptsAndBlockIfLimitExceeded(String cardNumber) {
        Session currentSession = sessionFactory.getCurrentSession();
        CreditCard nonBlockedCard = findNonBlockedCard(cardNumber);
        CardState cardState = nonBlockedCard.getCardState();
        if (cardState != null) {
            int attemptsCount = cardState.getWrongPinAttemptsCount() + 1;
            cardState.setWrongPinAttemptsCount(attemptsCount);
            boolean blocked = attemptsCount >= WRONG_PIN_ATTEMPTS_COUNT;
            if (blocked) {
                LOG.warn("block card with number [{}]", nonBlockedCard.getCardNumber());
                cardState.setBlocked(blocked);
            }
            currentSession.save(cardState);
            return blocked;
        }
        LOG.error("cannot find cardState for creditCard");
        throw new IllegalStateException("cannot find cardState for creditCard");
    }

    @Override
    public void resetWrongPinCodeCountAttempts(String cardNumber) {
        LOG.info("reset wrong pin code count attempts for car with number [{}]", cardNumber);
        Session currentSession = sessionFactory.getCurrentSession();
        CreditCard nonBlockedCard = findNonBlockedCard(cardNumber);
        CardState cardState = nonBlockedCard.getCardState();
        if (cardState != null) {
            cardState.setWrongPinAttemptsCount(0);
            currentSession.save(cardState);
            return;
        }
        LOG.error("cannot find cardState for creditCard");
        throw new IllegalStateException("cannot find cardState for creditCard");
    }

    @Override
    public void saveCard(CreditCard card) {
        LOG.info("saving credit card with number [{}]", card.getCardNumber());
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(card);
    }
}
