package com.dataart.cashmashine.dao;

import com.dataart.cashmashine.domain.CreditCard;

/**
 * Created by redwhite on 11.11.2015.
 */
public interface CardDao {
    CreditCard findNonBlockedCard(String cardNumber);

    String getPinByCardNumber(String cardNumber);

    boolean incrementWrongPinCodeCountAttemptsAndBlockIfLimitExceeded(String cardNumber);

    void resetWrongPinCodeCountAttempts(String cardNumber);

    void saveCard(CreditCard card);
}
