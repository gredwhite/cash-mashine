package com.dataart.cashmashine.utils;

/**
 * Created by redwhite on 11.11.2015.
 */
public enum PinCodeCheckResult {
    VALID,
    INVALID,
    BLOCKED
}
