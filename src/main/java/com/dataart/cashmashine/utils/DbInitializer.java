package com.dataart.cashmashine.utils;

import com.dataart.cashmashine.domain.CardState;
import com.dataart.cashmashine.domain.CreditCard;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by redwhite on 10.11.2015.
 */
@Component
public class DbInitializer {
    private static final Logger LOG = LoggerFactory.getLogger(DbInitializer.class);
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostConstruct
    public void init() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            CreditCard creditCard1 = new CreditCard();
            creditCard1.setBalance(12345);
            creditCard1.setCardNumber("1234512345123451");
            creditCard1.setPinCode(bCryptPasswordEncoder.encode("1234"));

            CardState cardState = new CardState();
            creditCard1.setCardState(cardState);

            session.save(creditCard1);

            CreditCard creditCard2 = new CreditCard();
            creditCard2.setBalance(1000);
            creditCard2.setCardNumber("7777777777777777");
            creditCard2.setPinCode(bCryptPasswordEncoder.encode("7777"));

            CardState cardState2 = new CardState();
            creditCard2.setCardState(cardState2);

            session.save(creditCard2);

            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            LOG.warn("could not initialize database [{}]", e);
        }
    }
}



