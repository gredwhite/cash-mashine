package com.dataart.cashmashine.domain;

import com.dataart.cashmashine.hibernateconverters.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by redwhite on 10.11.2015.
 */
@Table
@Entity
public class Operation {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;
    @Column
    @Enumerated(EnumType.STRING)
    private OperationType operationType;
    @Column
    private Long moneyAmount;

    @Column
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    LocalDateTime date = LocalDateTime.now();

    public Long getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(Long moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }


}
