package com.dataart.cashmashine.domain;

/**
 * Created by redwhite on 11.11.2015.
 */
public enum OperationType {
    WATCH_BALANCE,
    WITH_DRAW
}
