package com.dataart.cashmashine.domain;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by redwhite on 10.11.2015.
 */
@Entity
@Table
public class CardState {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;
    @Column
    private Integer wrongPinAttemptsCount = 0;
    @Column
    private boolean blocked;

    public Integer getWrongPinAttemptsCount() {
        return wrongPinAttemptsCount;
    }

    public void setWrongPinAttemptsCount(Integer wrongPinAttemptsCount) {
        this.wrongPinAttemptsCount = wrongPinAttemptsCount;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
