package com.dataart.cashmashine.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by redwhite on 10.11.2015.
 */
@Table
@Entity
public class CreditCard {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(unique = true, nullable = false, name = "card_id")
    Long id;
    @Column
    private String cardNumber;
    @Column
    private long balance;
    @Column
    private String pinCode;
    @OneToOne(cascade = CascadeType.ALL)
    private CardState cardState;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "card_id")
    private Set<Operation> operations = new HashSet<>();

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CardState getCardState() {
        return cardState;
    }

    public void setCardState(CardState cardState) {
        this.cardState = cardState;
    }

    public Set<Operation> getOperations() {
        return operations;
    }

    public void setOperations(Set<Operation> operations) {
        this.operations = operations;
    }
}

