package com.dataart.cashmashine.controller;

import com.dataart.cashmashine.domain.CreditCard;
import com.dataart.cashmashine.dto.BalanceAndLastOperationDto;
import com.dataart.cashmashine.service.CardService;
import com.dataart.cashmashine.utils.PinCodeCheckResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by redwhite on 11.11.2015.
 */
@Controller
public class CardController {
    private static final Logger LOG = LoggerFactory.getLogger(CardController.class);

    public static final String CARD_NUMBER_SESSION_KEY = "cardNumber";
    public static final String CREDIT_CARD = "creditCard";

    @Autowired
    private CardService cardService;

    /**
     * Initial application page.
     * If user which has already entered card will go by this url it will be redirected to input pin page.
     * If user which has already entered card and pin will go by this url it will be redirected to operation page.
     * Otherwise user will be redirected to page where he can input card number
     *
     * @param httpSession
     * @return corresponding view
     */
    @RequestMapping("/")
    public String inputCardNumber(HttpSession httpSession) {
        if (httpSession.getAttribute(CREDIT_CARD) != null) {
            return "operations";
        } else if (httpSession.getAttribute(CARD_NUMBER_SESSION_KEY) != null) {
            return "redirect:/inputPin";
        }
        return "inputCardNumber";
    }

    /**
     * Leads to the page where user inputs pin.
     * If user doesn't input card number before he will be redirected to inputCardNumber page.
     *
     * @param cardNumber
     * @param httpServletRequest
     * @param model
     * @return corresponding view
     */
    @RequestMapping(value = "/findCard", method = RequestMethod.POST)
    public String findCard(@RequestParam(value = "cardNumber", required = false) String cardNumber, HttpServletRequest httpServletRequest, Model model, HttpSession httpSession) {
        if (cardNumber == null) {
            return "inputCardNumber";
        }
        CreditCard nonBlockedCard = cardService.findNonBlockedCard(cardNumber);
        if (nonBlockedCard != null) {
            //open session if it doesn't exist
            HttpSession session = httpServletRequest.getSession(true);
            session.setAttribute("cardNumber", cardNumber);
            return "redirect:inputPin";
        }
        LOG.info("active card with number [{}] not found", cardNumber);
        model.addAttribute("cause", "active card not found");//in real code I suggest to use messageSource instead
        return "errorInput";
    }

    @RequestMapping(value = "/inputPin", method = RequestMethod.GET)
    public String inputPin(HttpSession httpSession) {
        if (httpSession.getAttribute(CREDIT_CARD) != null) {
            return "operations";
        }
        String cardNumber = (String) httpSession.getAttribute(CARD_NUMBER_SESSION_KEY);
        if (cardNumber == null) {
            return "redirect:/";
        }
        return "inputPin";

    }

    /**
     * Method checks pin. If it correct user forwards to operations page.
     * If pin is not correct - user forwards to error page.
     * If incorrect pin limit exceeded - card blocks
     *
     * @param pinCode
     * @param httpSession
     * @param model
     * @return corresponding view
     */
    @RequestMapping(value = "/checkPinCode", method = RequestMethod.POST)
    public String checkPinCode(@RequestParam(value = "pinCode") String pinCode, HttpSession httpSession, Model model) {
        String cardNumber = (String) httpSession.getAttribute(CARD_NUMBER_SESSION_KEY);
        if (cardNumber == null) { // if card number does not contain in session we can not check that pin is correct
            return "redirect:/findCard";
        }
        PinCodeCheckResult pinCodeCheckResult = cardService.checkPinForCard(pinCode, cardNumber);
        switch (pinCodeCheckResult) {
            case VALID:
                //we suppose that no one can access to database and cannot remove line after starting session.
                httpSession.setAttribute(CREDIT_CARD, cardService.findNonBlockedCard(cardNumber));
                return "operations";
            case INVALID:
                model.addAttribute("cause", "invalid pin");
                return "errorInput";
            default:
                return "blocked";
        }
    }

    @RequestMapping(value = "/balance", method = RequestMethod.GET)
    public String balance(HttpSession httpSession) {
        CreditCard creditCard = (CreditCard) httpSession.getAttribute(CREDIT_CARD);
        if (creditCard == null) {
            return "redirect:/findCard";
        }
        LOG.info("Balance requested for card [{}]", creditCard.getCardNumber());
        cardService.trackBalanceRequest(creditCard.getCardNumber());
        return "balance";
    }

    @RequestMapping(value = "/successCashOut", method = RequestMethod.GET)
    public String successCashOut() {
        return "successCashOut";
    }

    @RequestMapping(value = "/cashOut", method = RequestMethod.GET)
    public String cahOut() {
        return "cashOut";
    }

    @RequestMapping(value = "/cashOut", method = RequestMethod.POST)
    public String cahOut(@RequestParam(value = "amount") Long amount, Model model, HttpSession httpSession, RedirectAttributes redirectAttributes) {
        CreditCard creditCardInSession = (CreditCard) httpSession.getAttribute(CREDIT_CARD);
        if (amount == null) {
            LOG.warn("Wrong amount typed for card [{}]", creditCardInSession.getCardNumber());
            model.addAttribute("cause", "wrong amount typed");
            return "errorInput";
        }
        BalanceAndLastOperationDto balanceAndLastOperationDto = cardService.withDraw(creditCardInSession.getBalance(), amount, creditCardInSession.getCardNumber());
        if (balanceAndLastOperationDto != null) {
            creditCardInSession.setBalance(balanceAndLastOperationDto.getNewBalance());
            redirectAttributes.addFlashAttribute("operation", balanceAndLastOperationDto.getOperation());
            return "redirect:successCashOut"; // to avoid duplicated withdraw after page reload
        }
        LOG.warn("Attempt to withdraw too large sum for card [{}]", creditCardInSession.getCardNumber());
        model.addAttribute("cause", "amount is too large");
        return "errorInput";
    }

    @RequestMapping(value = "/exit", method = RequestMethod.GET)
    public String exit(HttpSession httpSession) {
        CreditCard creditCardInSession = (CreditCard) httpSession.getAttribute(CREDIT_CARD);
        if (creditCardInSession != null && creditCardInSession.getCardNumber() != null) {
            LOG.warn("Session for card [{}] expired by user request", creditCardInSession.getCardNumber());
        }
        httpSession.invalidate();
        return "redirect:/";
    }


}
