<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <script src="//code.jquery.com/jquery-1.8.0.js"></script>
    <script src="/resources/js/backButton.js"></script>
    <script src="/resources/js/exitButton.js"></script>
</head>

<body>

<h3>success cash out</h3>

card number: ${creditCard.cardNumber} <br>
date: ${operation.date} <br>
new balance : ${creditCard.balance} <br>
withdraw amount : ${operation.moneyAmount} <br>

<jsp:include page="components/exitButton.jsp"/>
<jsp:include page="components/backButton.jsp"/>

</body>
</html>