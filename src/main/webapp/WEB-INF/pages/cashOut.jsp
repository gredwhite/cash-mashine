<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <script src="//code.jquery.com/jquery-1.8.0.js"></script>
    <script src="/resources/js/clearButton.js"></script>
    <script src="/resources/js/exitButton.js"></script>
</head>

<body>
<form action="/cashOut" method="post">
    <input type="number" name="amount" id="amount"/>
    <input type="submit" value="ok"/> <br> <br>
    <jsp:include page="components/clearButton.jsp"/>
    <br>
</form>
<jsp:include page="components/exitButton.jsp"/>

</body>
</html>