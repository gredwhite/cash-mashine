<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title></title>
    <script src="//code.jquery.com/jquery-1.8.0.js"></script>
    <script src="/resources/js/backButton.js"></script>
    <script src="/resources/js/exitButton.js"></script>

</head>
<body>

<jsp:useBean id="now" class="java.util.Date"/>

balance: ${creditCard.balance} <br>
card number: ${creditCard.cardNumber} <br>
date: <fmt:formatDate value="${now}" pattern="dd-MM-yyyy HH:mm:ss a z"/> <br> <br>

<jsp:include page="components/exitButton.jsp"/> <br> <br>
<jsp:include page="components/backButton.jsp"/>

</body>

</html>