<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <script src="//code.jquery.com/jquery-1.8.0.js"></script>
    <script src="/resources/js/backButton.js"></script>
</head>
<body>
<h3>Error</h3>

message: ${cause} <br> <br>

<jsp:include page="components/backButton.jsp"/>

</body>
</html>
