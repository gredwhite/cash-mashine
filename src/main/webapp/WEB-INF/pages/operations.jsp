<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <script src="//code.jquery.com/jquery-1.8.0.js"></script>
    <script src="/resources/js/exitButton.js"></script>
    <script src="/resources/js/cashOutButton.js"></script>
    <script src="/resources/js/balanceButton.js"></script>

</head>
<body>

<jsp:include page="components/balanceButton.jsp"/> <br> <br>
<jsp:include page="components/cashOutButton.jsp"/> <br> <br>
<jsp:include page="components/exitButton.jsp"/> <br> <br>

</body>
</html>
