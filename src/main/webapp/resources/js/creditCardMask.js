$(function () {
    $.mask.definitions['~'] = '[+-]';
    $('.card').mask('9999-9999-9999-9999');

    $("#findCardForm").on("submit", function () {
        $(this).find(".card").val($(this).find(".card").val().replace(/-/g, ''));
    })
});
