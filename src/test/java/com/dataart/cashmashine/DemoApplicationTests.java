package com.dataart.cashmashine;

import com.dataart.cashmashine.controller.CardController;
import com.dataart.cashmashine.domain.CreditCard;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
@WebAppConfiguration
public class DemoApplicationTests {
    private MockMvc mvc;

    @Before
    public void setUp() throws Exception {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/view/");
        viewResolver.setSuffix(".jsp");
        mvc = MockMvcBuilders.standaloneSetup(
                new CardController())
                .setViewResolvers(viewResolver)
                .build();
    }

    @Test
    public void ShouldLeadToTheInputCardNumberPageTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("inputCardNumber"));
    }

    @Test
    public void rootShouldLeadToTheInputPinPageTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/")
                .sessionAttr(CardController.CARD_NUMBER_SESSION_KEY, "1234512345123451")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is(302))
                .andExpect(view().name("redirect:/inputPin"));
    }

    @Test
    public void shouldLeadToOperationsPageTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/")
                .sessionAttr(CardController.CREDIT_CARD, mock(CreditCard.class))
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("operations"));
    }

    @Test
    public void exitTest() throws Exception {
        HttpSession session = mvc.perform(MockMvcRequestBuilders
                .get("/exit")
                .sessionAttr(CardController.CREDIT_CARD, mock(CreditCard.class))
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is(302))
                .andExpect(view().name("redirect:/"))
                .andReturn()
                .getRequest()
                .getSession();

        assertNull(session.getAttribute(CardController.CREDIT_CARD));

    }

    @Test
    public void shouldLeadToTheOperationsPageInputPinTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/inputPin")
                .sessionAttr(CardController.CREDIT_CARD, mock(CreditCard.class))
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is(200))
                .andExpect(view().name("operations"));
    }

    @Test
    public void shouldLeadToTheInputPinInputPinTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/inputPin")
                .sessionAttr(CardController.CARD_NUMBER_SESSION_KEY, "1234512345123451")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is(200))
                .andExpect(view().name("inputPin"));
    }

    @Test
    public void shouldLeadToTheInputCardNumberPageInputPinTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/inputPin")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is(302))
                .andExpect(view().name("redirect:/"));
    }

}
