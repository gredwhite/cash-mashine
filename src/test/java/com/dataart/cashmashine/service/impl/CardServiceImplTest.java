package com.dataart.cashmashine.service.impl;

import com.dataart.cashmashine.dao.CardDao;
import com.dataart.cashmashine.domain.CreditCard;
import com.dataart.cashmashine.domain.Operation;
import com.dataart.cashmashine.domain.OperationType;
import com.dataart.cashmashine.dto.BalanceAndLastOperationDto;
import com.dataart.cashmashine.utils.PinCodeCheckResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CardServiceImplTest {
    @Mock
    CardDao cardDaoMock;
    @Mock
    BCryptPasswordEncoder bCryptPasswordEncoderMock;
    @Spy
    @InjectMocks
    CardServiceImpl cardService = new CardServiceImpl();

    @Test
    public void shouldReturnBlockedCheckPinForCardTest() throws Exception {
        when(cardDaoMock.incrementWrongPinCodeCountAttemptsAndBlockIfLimitExceeded("11112222333344445")).thenReturn(true);
        when(cardDaoMock.getPinByCardNumber("11112222333344445")).thenReturn("encodedPin");
        when(bCryptPasswordEncoderMock.matches("1234", "encodedPin")).thenReturn(false);

        PinCodeCheckResult pinCodeCheckResult = cardService.checkPinForCard("1234", "11112222333344445");

        assertSame(PinCodeCheckResult.BLOCKED, pinCodeCheckResult);
    }

    @Test
    public void shouldReturnInvalidCheckPinForCardTest() throws Exception {
        when(cardDaoMock.incrementWrongPinCodeCountAttemptsAndBlockIfLimitExceeded("11112222333344445")).thenReturn(false);
        when(cardDaoMock.getPinByCardNumber("11112222333344445")).thenReturn("encodedPin");
        when(bCryptPasswordEncoderMock.matches("1234", "encodedPin")).thenReturn(false);

        PinCodeCheckResult pinCodeCheckResult = cardService.checkPinForCard("1234", "11112222333344445");

        assertSame(PinCodeCheckResult.INVALID, pinCodeCheckResult);
    }

    @Test
    public void shouldReturnValidCheckPinForCardTest() throws Exception {
        when(cardDaoMock.getPinByCardNumber("11112222333344445")).thenReturn("encodedPin");
        when(bCryptPasswordEncoderMock.matches("1234", "encodedPin")).thenReturn(true);

        PinCodeCheckResult pinCodeCheckResult = cardService.checkPinForCard("1234", "11112222333344445");

        assertSame(PinCodeCheckResult.VALID, pinCodeCheckResult);
        verify(cardDaoMock).resetWrongPinCodeCountAttempts("11112222333344445");
    }

    @Test
    public void shouldReturnNullIfBalanceIsNotEnoughWithDrawTest() throws Exception {
        BalanceAndLastOperationDto balanceAndLastOperationDto = cardService.withDraw(100, 101, "11112222333344445");

        assertEquals(null, balanceAndLastOperationDto);
    }

    @Test
    public void shouldReturnFilledDtoIfBalanceIsEnoughWithDrawTest() throws Exception {
        ArgumentCaptor<CreditCard> creditCardArgumentCaptor = ArgumentCaptor.forClass(CreditCard.class);
        CreditCard creditCard = new CreditCard();
        creditCard.setBalance(100);
        when(cardDaoMock.findNonBlockedCard("11112222333344445")).thenReturn(creditCard);
        doNothing().when(cardDaoMock).saveCard(creditCardArgumentCaptor.capture());

        BalanceAndLastOperationDto balanceAndLastOperationDto = cardService.withDraw(100, 100, "11112222333344445");

        CreditCard creditCardArgumentCaptorValue = creditCardArgumentCaptor.getValue();
        assertEquals(Long.valueOf(0), Long.valueOf(creditCardArgumentCaptorValue.getBalance()));
        assertNotNull(balanceAndLastOperationDto);


        Set<Operation> operations = creditCard.getOperations();
        assertEquals(1, operations.size());
        Operation operation = operations.iterator().next();
        assertEquals(OperationType.WITH_DRAW, operation.getOperationType());
        assertEquals(Long.valueOf(100L), operation.getMoneyAmount());

        assertEquals(Long.valueOf(0L), Long.valueOf(balanceAndLastOperationDto.getNewBalance()));
        assertSame(operation, balanceAndLastOperationDto.getOperation());
    }
}